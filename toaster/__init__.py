# To support reload properly, try to access a package var, 
# if it's there, reload everything
import bpy
from bpy.types import Menu, Panel, UIList
from . import column

bl_info = {
    'name': 'toaster',
    'author': 'Michael Demetriou',
    'location': 'View3D > UI panel > Toaster',
    'category': '3D View',
    'support': 'TESTING',
    }

#check if F8 was pressed and if yes reload modules
if "bpy" in locals():
    import imp
    imp.reload(column)
    print("Reloaded modules")
else:
    print("Imported modules")
    
#a collection of icons for our add-on    
preview_collections = {}

#This is the operator for adding primitives
class OBJECT_OT_AddButton(bpy.types.Operator):
    bl_idname = "toaster.add"
    bl_label = "Add"
    mesh = bpy.props.StringProperty()
 
    def execute(self, context):
        bpy.ops.mesh.primitive_cube_add(location=(0, 0, 0))
        if self.mesh == "column":
            bpy.ops.transform.resize(value=(1, 1, 4))
        elif self.mesh == "column-external":
            column.makeMesh(-8)
        elif self.mesh == "beam":
            context.scene.objects.active.dimensions = (6, 2, 2)
        return{'FINISHED'}    
    
#A new panel in the tools section of the 3D view
class View3DPanel(): 
    bl_space_type = 'VIEW_3D' 
    bl_region_type = 'TOOLS'

class ToasterPanel(View3DPanel, Panel):
    bl_label = "Members" 
    bl_context = "objectmode" 
    bl_category = "toaster"
    
    def draw(self, context): 
        layout = self.layout 
        col = layout.column(align=True) 
        pcoll = preview_collections["main"]
        col.operator("toaster.add", text="Column", icon_value=pcoll["column"].icon_id).mesh="column"
        col.operator("toaster.add", text="Beam", icon_value=pcoll["beam"].icon_id).mesh="beam"
        
def register(): 

    import bpy.utils.previews #icons module
    import os
    
    pcoll = bpy.utils.previews.new()

    # path to the folder where the icons are
    script_path = __file__
    my_icons_dir = os.path.join(os.path.dirname(script_path), "icons/")

    # load icons from files and store in the previews collection with the same name
    for file in os.listdir(my_icons_dir):
     if os.path.isfile(os.path.join(my_icons_dir,file)) and file.endswith('.png'):
        pcoll.load(file.replace('.png',''), os.path.join(my_icons_dir, file), 'IMAGE')

    preview_collections["main"] = pcoll

    bpy.utils.register_module(__name__)

def unregister(): 
    
    bpy.utils.unregister_module(__name__)
    
    for pcoll in preview_collections.values():
        bpy.utils.previews.remove(pcoll)
    preview_collections.clear()

if __name__ == "__main__": 
    register()

print("Toaster Loaded")
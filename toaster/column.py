#----------------------------------------------------------
# File column.py
#
# This is not used, just here to show how to
# import external files
#----------------------------------------------------------
import bpy
 
def makeMesh(z):
    bpy.ops.mesh.primitive_cube_add(location=(0,0,z))
    bpy.ops.transform.resize(value=(1,1,4))
    return bpy.context.object
 
if __name__ == "__main__":
    ob = makeMesh(1)
    print(ob, "created")
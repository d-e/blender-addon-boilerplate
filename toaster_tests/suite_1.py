import bpy
import unittest
import toaster

from mathutils import Vector

class ToasterMeshTests(unittest.TestCase):
    def context_override(self):
        override = bpy.context.copy()
        for window in bpy.context.window_manager.windows:
            screen = window.screen
            for area in screen.areas:
                if area.type == 'VIEW_3D':
                    override['window'] = window
                    override['screen'] = screen
                    override['area'] = area
                    override['region'] = [region for region in area.regions if region.type == 'WINDOW'][0]
                    override['selected_objects'] = [o for o in bpy.context.scene.objects if o.select]
                    override['active_object'] = bpy.context.scene.objects.active
                    return override

    @staticmethod
    def is_inside(p, max_dist, obj):
        # max_dist = 1.84467e+19
        result, point, normal, face = obj.closest_point_on_mesh(p, max_dist)
        p2 = point - p
        v = p2.dot(normal)
        print(v)
        return not (v < 0.0)

    def setUp(self):
        if bpy.data.objects.get('Cube') is not None:
            bpy.data.objects.remove(bpy.data.objects['Cube'], do_unlink=True)

    def tearDown(self):
        pass

    def test_addon_enabled(self):
        self.assertIsNotNone(toaster.bl_info)

    def test_add_column(self):
        bpy.context.scene.cursor_location = (3, 3.275, 0)
        bpy.ops.toaster.add(mesh="column")
        self.assertIsNotNone(bpy.data.objects['Cube'], "No column created")
        self.assertEquals(bpy.data.objects['Cube'].location, Vector((0,0,0)), "Column not at the correct position")

    def test_add_beam(self):
        bpy.context.scene.cursor_location = (3, 3.275, 0)
        bpy.ops.toaster.add(self.context_override(), mesh="beam")
        self.assertIsNotNone(bpy.data.objects['Cube'], "No beam created")
        self.assertEquals(bpy.data.objects['Cube'].location, Vector((0,0,0)), "Beam not at the correct position")


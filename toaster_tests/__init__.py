import bpy
from bpy.types import Menu, Panel, UIList
from .suite_1 import ToasterMeshTests
import unittest

bl_info = {
    'name': 'toaster_tests',
    'author': 'Michael Demetriou',
    'location': 'View3D > UI panel > toaster_tests',
    'category': '3D View',
    'support': 'TESTING',
    }

#check if F8 was pressed and if yes reload modules
if "bpy" in locals():
    import imp
    imp.reload(suite_1)
    print("Reloaded modules")
else:
    print("Imported modules")

    class TestsOperator(bpy.types.Operator):
        bl_idname = "toaster_tests.run"
        bl_label = "Toaster Tests"
        bl_options = {'REGISTER', 'UNDO'}
        testcase = bpy.props.StringProperty()

        def execute(self, context):
            if self.testcase == "all":
                suite = unittest.defaultTestLoader.loadTestsFromTestCase(ToasterMeshTests)
            else:
                suite = unittest.TestSuite()
                suite.addTest(ToasterMeshTests(self.testcase))
            unittest.TextTestRunner().run(suite)
            return {'FINISHED'}


# And the UI panel

# A new panel in the tools section of the 3D view
class View3DPanel():
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'


class ToasterTestsPanel(View3DPanel, Panel):
    bl_label = "Toaster Tests"
    bl_context = "objectmode"
    bl_category = "Toaster Tests"

    def draw(self, context):
        layout = self.layout
        col = layout.column(align=True)
        col.operator("toaster_tests.run").testcase = "all"
        col = layout.column(align=True)
        col.operator("toaster_tests.run", text="Test 1").testcase = "test_add_column"
        col.operator("toaster_tests.run", text="Test 2").testcase = "test_add_beam"


def register():
    bpy.utils.register_module(__name__)

def unregister():
    bpy.utils.unregister_module(__name__)


if __name__ == "__main__":
    register()
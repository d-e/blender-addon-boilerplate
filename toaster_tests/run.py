#!/usr/bin/python

import unittest
from toaster_tests.suite_1 import ToasterMeshTests
import sys

def run():
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(ToasterMeshTests)
    success = unittest.TextTestRunner().run(suite).wasSuccessful()
    if not success:
        raise Exception('Tests Failed')

try:
    run()
    sys.exit(0)
except Exception:
    sys.exit(1)

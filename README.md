# Toaster &ndash; A blender addon boilerplate
## clone this and start coding blender addons right away

This repository includes a .blend file and a multi-file add on. The goal
is to be able to start coding right away, without having to actually install 
a basic blender addon.

This allows you to code from whatever directory you want and not only from 
the blender dotdir. It also allows you to use an external editor or the 
internal blender editor, whatever suits you most. I have already included 
`setup.py` and `nb-configuration.xml` so you can open it with **netbeans** 
if you have the python addon installed (this doesn't seem to work on the mac 
at the time of writing)

Unfortunately I didn't manage to make it reload using F8, any advice is 
welcome on that matter. You will have to press the **Run Script** button
on the top left every time you change your code.

![screenshot](screenshot-run.png)

The addon also adds a panel so that it is immediately obvious that everything works and
that external modules are loaded. Adding more GUI elements or options is out
of the scope of this boilerplate.

Of course, credits to the [blender cookbook](https://wiki.blender.org/index.php/Dev:Py/Scripts/Cookbook/Code_snippets/Multi-File_packages)

## Unit tests and Continuous Integration

Now toaster contains unit tests and is ready for **continuous integration** with *GitLab*. It should be trivial
for this configuration to be modified to work with other CI cystems